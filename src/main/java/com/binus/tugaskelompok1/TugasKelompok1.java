/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.binus.tugaskelompok1;
import java.util.Scanner;
import java.text.DecimalFormat;
/**
 *
 * @author rijal
 */
public class TugasKelompok1 {

    public static void main(String[] args) {
       	
		int jml_pesanan,nasi_goreng,ayam_bakar,steak_sirloin,kwetiaw_siram,kambing_guling;
		double[] harga={9999.99,12345.67,21108.40,13579.13,98765.43};
		double total_1 = 0,total_2 = 0,total_3 = 0,total_4 = 0,total_5 = 0;
		double total_awal,disc,total_pembelian,total_orang;
		String namapemesan;
		DecimalFormat dc = new DecimalFormat("#.##");
                
                Scanner input = new java.util.Scanner(System.in);
		
	
		System.out.println("\t     Selamat Datang di Restoran Bungar!"
	                + "\n======================================================="	                
	                + "\n\t\t     Promo Spesial diskon 10%"
	                +"\n=======================================================");
                
                System.out.println("Selamat Siang ... ");
                
		System.out.print("Pesan untuk berapa orang : ");
                while (!input.hasNextInt()) {
			System.out.println("Hanya bisa menerima inputan bilangan bulat.\n");
			System.out.print("Pesan untuk berapa orang : ");
			input.next();
		}
		jml_pesanan=input.nextInt();
                input.nextLine();
		System.out.print("Pesanan atas nama : ");
		namapemesan=input.nextLine();
		System.out.println();
		
		System.out.println("Menu Spesial Hari ini");
		System.out.println("===================== \n");
		System.out.println("1. Nasi Goreng Spesial 			@ Rp.  9999,99 ");
		System.out.println("2. Ayam Bakar Spesial			@ Rp. 12345,67 ");
		System.out.println("3. Steak Sirloin Spesial		@ Rp. 21108,40 ");
		System.out.println("4. Kwetiaw Siram Spesial		@ Rp. 13579,13 ");
		System.out.println("5. Kambing Guling Spesial		@ Rp. 98765,43 \n ");
		
		
		System.out.printf("Menu Spesial hari ini \n");
                System.out.println("===================== \n");
                System.out.println("Keterangan [Pilih Menu dengan memasukan angka / pilih 0 untuk tidak memesan] \n");
                System.out.println("===================== \n");
		System.out.print("1. Nasi Goreng Spesial		= ");
		nasi_goreng=input.nextInt();
		total_1= (nasi_goreng * harga[0]);
		System.out.print("2. Ayam Bakar Spesial		= ");
		ayam_bakar=input.nextInt();
		total_2 = (ayam_bakar * harga[1]);
		System.out.print("3. Steak Sirloin Spesial	= ");
		steak_sirloin=input.nextInt();
		total_3 = (steak_sirloin * harga[2]);
		System.out.print("4. Kwetiaw Siram Spesial	= ");
		kwetiaw_siram=input.nextInt();
		total_4 = (kwetiaw_siram * harga[3]);
		System.out.print("5. Kambing Guling Spesial	= ");
		kambing_guling=input.nextInt();
		total_5 = (kambing_guling * harga[4]);
                
		System.out.println("\n"+ "Selamat menikmati makanan anda..... " + "\n");
		
		total_awal = (total_1 + total_2 + total_3 + total_4 + total_5);
		disc=(total_awal * 0.1);
		total_pembelian = (total_awal- disc);
		total_orang = (total_pembelian / jml_pesanan);
		
		System.out.println("Total Pembelian : ");
		System.out.println("1. Nasi Goreng Spesial		" + nasi_goreng + " Porsi * Rp.  9999,99  = Rp. " + dc.format(total_1));
		System.out.println("2. Ayam Bakar Spesial		" + ayam_bakar + " Porsi * Rp. 12345,67  = Rp. " + dc.format(total_2));
		System.out.println("3. Steak Sirloin Spesial	" + steak_sirloin + " Porsi * Rp. 21108,40  = Rp. " + dc.format(total_3));
		System.out.println("4. Kwetiaw Siram Spesial	" + kwetiaw_siram + " Porsi * Rp. 13579,13  = Rp. " + dc.format(total_4));
		System.out.println("5. Kambing Guling Spesial 	" + kambing_guling + " Porsi * Rp. 98765,43  = Rp. " + dc.format(total_5));
		System.out.println("=========================================================================== +");
		System.out.println("Total Pembelian					= Rp. " + dc.format(total_awal));
		System.out.println("Disc 10% (Masa Promosi)				= Rp. " + dc.format(disc));
                System.out.println("=========================================================================== -");
		System.out.println("Total Pembelian Setelah Disck 10%               = Rp. " + dc.format(total_pembelian));
		System.out.println("Pembelian per orang	< untuk" + jml_pesanan + "orang >		= Rp. " + dc.format(total_orang));
		System.out.println("");
		
		System.out.println("------------------------Terimakasih Atas Kunjungan Anda--------------------------");
		System.out.println("			  Tekan ENTER untuk keluar ..");
	
    }
}
